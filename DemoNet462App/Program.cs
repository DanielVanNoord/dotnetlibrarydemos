﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoNet462App
{
    class Program
    {
        //Net 462 can use all the libraries
        static void Main(string[] args)
        {
            Console.WriteLine(DemoNet35Lib.Squack.Say("Net462"));
            Console.WriteLine(DemoNetStan20Lib.Squack.Say("Net462"));
            Console.WriteLine(DemoNetStan20AndNet35Lib.Squack.Say("Net462"));
            Console.ReadLine();
        }
    }
}
