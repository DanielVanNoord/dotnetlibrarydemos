﻿using System;

namespace DemoNetStan20Lib
{
    //This is a "Pure" Net Standard library, it is callable from any .Net instance (or higher versions of those) on the runtime list
    public static class Squack
    {
        public static string Say(string appSource)
        {
            return string.Format("Hello {0}, you have called a Net Standard Library.", appSource);
        }
    }
}
