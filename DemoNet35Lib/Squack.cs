﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DemoNet35Lib
{
    //This is a "Pure" NetFx 3.5 library, it is callable from any .Net Fx 3.5+ library or application
    public static class Squack
    {
        public static string Say(string appSource)
        {
            return string.Format("Hello {0}, you have called a Net 3.5 Library.", appSource);
        }
    }
}
