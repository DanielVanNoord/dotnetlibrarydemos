﻿using System;

namespace DemoNetStan20AndNet35Lib
{
    //This project builds both a NetStan 2.0 and a NetFx 3.5 library (right click and say Edit ****.csproj to see how this is setup).
    //It is callable from NetFx 3.5+ or any NetStandard 2.0+ compatible runtime.
    //Normally this would be distributed in a Nuget package with both dlls. Nuget then handles delivering the correct one to the calling app.
    //Because I don't fell like doing that for a demo I will directly reference this project, which can cause issues for libraries that can call either (like net461+)
    //Because when offered both msbuild sometimes can't decide. This would not be a problem in a proper nuget distribution.

    //Any code here that would not work in both targets would need to be ifdefed out and replaced for the other runtime (see Rays example blog post about audio library)
    public static class Squack
    {
        public static string Say(string appSource)
        {
            return string.Format("Hello {0}, you have called a Net Standard Library / Net35 Library.", appSource);
        }
    }
}
