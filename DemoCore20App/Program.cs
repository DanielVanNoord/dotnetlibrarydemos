﻿using System;

namespace DemoCore20App
{
    class Program
    {
        //Net Core can't use Pure NetFx
        static void Main(string[] args)
        {
            Console.WriteLine(DemoNetStan20Lib.Squack.Say("Net Core"));
            Console.WriteLine(DemoNetStan20AndNet35Lib.Squack.Say("Net Core"));
            Console.ReadLine();
        }
    }
}
