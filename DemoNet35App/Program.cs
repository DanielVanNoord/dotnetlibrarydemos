﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DemoNet35App
{
    class Program
    {
        //Net 35 can't use pure NetStandard
        static void Main(string[] args)
        {
            Console.WriteLine(DemoNet35Lib.Squack.Say("Net35"));
            Console.WriteLine(DemoNetStan20AndNet35Lib.Squack.Say("Net35"));
            Console.ReadLine();
        }
    }
}
